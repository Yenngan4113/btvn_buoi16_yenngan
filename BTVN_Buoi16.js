// Bài 1
document.getElementById("bai_1").addEventListener("click", function () {
  n = 1;
  sum = 0;
  for (n; sum < 10000; n++) {
    sum += n;
  }
  document.getElementById(
    "ket_qua_bai_1"
  ).innerHTML = `Số dương nhỏ nhất n là: ${--n}`;
});
// Bài 2
document.getElementById("bai_2").addEventListener("click", function () {
  var x = document.getElementById("so_x").value * 1;
  var n = document.getElementById("so_n").value * 1;
  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum += x ** i;
  }
  document.getElementById("ket_qua_bai_2").innerHTML = `Tổng là: ${sum}`;
});
// Bài 3
document.getElementById("bai_3").addEventListener("click", function () {
  var a = document.getElementById("so_a").value * 1;
  var A = 1;
  for (var i = 1; i <= a; i++) {
    A *= i;
  }
  document.getElementById("ket_qua_bai_3").innerHTML = `Giai thừa là: ${A}`;
});
// Bài 4
document.getElementById("bai_4").addEventListener("click", function () {
  var contentHTML = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 != 0) {
      var contentLe = `<div class="bg-primary"> Div lẻ số ${i} <br/></div>`;
      contentHTML += contentLe;
    } else {
      var contentChan = `<div class="bg-danger"> Div chẵn số ${i} <br/></div>`;
      contentHTML += contentChan;
    }
  }
  document.getElementById("ket_qua_bai_4").innerHTML = `${contentHTML}
    `;
});
